<?php
/**
 * processFiles.php
 * Date: 1/10/13
 * Time: 5:19 PM
 */

require_once 'requires.php';

switch ($_REQUEST['action']) {
    case 'commit':
        libgit::commit();
        break;

    case 'checkout':
        libgit::checkout();
        break;

    // show a git diff (obsolete)
    case 'diff':
        libgit::diff();
        break;

    // show a git diff via AJAX
    case 'diffajax':
        libgit::diffajax();
        break;

    // show the diff between a null file and a new file
    case 'diffnull':
        libgit::diffnull();
        break;

    // checkout our version for unmerged files
    case 'ours':
        libgit::ours();
        break;

    // checkout their version for unmerged files
    case 'theirs':
        libgit::theirs();
        break;

    // commit the resolved file after conflict
    case 'commitresolv':
        libgit::commit('-i');
        break;

    // remove the untracked file
    case 'delete':
        libgit::delete();
        break;

    // resolve (actually "git add <file>")
    case 'resolve':
        libgit::resolve();
        break;

}

header("Location: ".config::$webPath);