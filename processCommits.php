<?php
/**
 * processCommits.php
 * Date: 1/11/13
 * Time: 2:41 PM
 */

require_once 'requires.php';

switch ($_POST['action']) {
    case 'pull':
        libgit::pull();
        break;

    case 'push':
        libgit::push();
        break;
}

header("Location: ".config::$webPath);
