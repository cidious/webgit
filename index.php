<?php
require_once 'requires.php';

if (empty($_SESSION['alert'])) {
    $_SESSION['alert'] = array();
}

$statusOut = libgit::status();
$logOut = array();
try {
    $logLocal  = libgit::log(true);
    $logServer = array_slice(libgit::log(false), 0, 10);
} catch (Exception $e) {
    $_SESSION['alert'][] = $e->getMessage();
}


include 'templates/index.phtml';
