<?php
/**
 * libgit
 * Date: 12/30/12
 * Time: 1:23 PM
 */ 
class libgit
{
    public static function parseOutput($command, $splitBySpace=true)
    {
        if (empty(config::$workDir) && !empty(config::$projects)) {
            if (empty($_SESSION['currentProject'])) {
                list($name, $arr) = each(config::$projects);
                reset(config::$projects);
                $_SESSION['currentProject'] = $name;
            }
            $_SESSION['workDir'] = config::$projects[$_SESSION['currentProject']]['workDir'];
            $_SESSION['remote']  = config::$projects[$_SESSION['currentProject']]['remote'];
            $_SESSION['branch']  = config::$projects[$_SESSION['currentProject']]['branch'];
        } else {
            $_SESSION['workDir'] = config::$workDir;
            $_SESSION['remote']  = config::$remote;
            $_SESSION['branch']  = config::$branch;
        }

        if (!is_dir($_SESSION['workDir'])) {
            $_SESSION['dialog'] = array(
                $_SESSION['workDir']." seems to be absent. Check the config and try again."
            );
        }
        @chdir($_SESSION['workDir']);
        exec($command." 2>&1", $outArr);
        if ($splitBySpace) {
            $retArr = array();
            foreach ($outArr as $string) {
                $retArr[] = str_getcsv(trim($string), ' ', '"');
            }
        } else {
            $retArr =& $outArr;
        }

        return $retArr;
    }

    public static function status()
    {
        $execArr = self::parseOutput('git status -s');
        $conflArr = self::parseOutput('git diff --name-only --diff-filter=U', false);

        $outArr = array();
        foreach ($execArr as $oneArr) {
            $status = array_shift($oneArr);
//            $name = implode(' ', $oneArr);
            $name = array_shift($oneArr);
            while ('' == $name) {
                $name = array_shift($oneArr);
            }
            if ('R' == $status[0]) {
                $arrow = array_shift($oneArr);
                $name  = array_shift($oneArr);
            }
            $outArr[] = array(
                'status'  => htmlspecialchars($status),
                'name'    => htmlspecialchars(trim($name)),
                'conflict'=> in_array(trim($name), $conflArr),
            );
        }

        return $outArr;
    }

    public static function log($local=true)
    {
        self::parseOutput('git fetch');

        if ($local) {
            $command = 'git log --format=fuller --date=iso '.
                $_SESSION['remote'].'/'.$_SESSION['branch'].'..'.$_SESSION['branch'];
        } else {
            $command = 'git log --format=fuller --date=iso '.
                $_SESSION['remote'].'/'.$_SESSION['branch'];
        }

        $outArr = self::parseOutput($command);

        $outStr = array();

        if (!is_array($outArr)) {
            throw new Exception('git log is empty');
        }
        if ($outArr && $outArr[0] && $outArr[0][0] == 'fatal:') {
            throw new Exception(implode(' ', $outArr[0]));
        }

        while(count($outArr)) {
            $commit = array_shift($outArr);
            if (!$commit) {
                continue;
            }
            $line = array_shift($outArr);
            if ($line[0] != 'Author:') {
                $line = array_shift($outArr);
            }
            $authorName = $line;
            $authorDate = array_shift($outArr);
            $commitName = array_shift($outArr);
            $commitDate = array_shift($outArr);
            $message = array();
            array_shift($outArr);
            while (count($outArr)) {
                $msg = trim(implode(' ', $tmp=array_shift($outArr)));
                if ($msg == '') {
                    break;
                }
                if (substr($msg, 0, 7) == 'commit ') {
                    array_unshift($outArr, $tmp);
                    break;
                }
                $message[] = $msg;
            }
            $messageStr = implode("\n", $message);

            array_shift($authorName);
            array_shift($authorDate);
            array_shift($commitName);
            array_shift($commitDate);
            $outStr[] = array(
                'commit'     => $commit[1],
                'authorName' => implode(' ', $authorName),
                'authorDate' => implode(' ', $authorDate),
                'commitName' => implode(' ', $commitName),
                'commitDate' => implode(' ', $commitDate),
                'message'    => $messageStr,
            );

        }
        return $outStr;
    }

    public static function makeFileString()
    {
        $fileArr = array();
        if (empty($_POST['file']) && !empty($_GET['file'])) {
            $fileArr = array($_GET['file']);
        }
        if (!empty($_POST['file']) && is_array($_POST['file'])) {
            $fileArr =& $_POST['file'];
        }
        array_walk($fileArr, function(&$val, &$key) {
            $val = htmlspecialchars_decode($val);
        });
        return '"'.implode('" "', $fileArr).'"';
    }

    public static function commit($option='')
    {
        chdir($_SESSION['workDir']);
        exec("git add -A 2>&1", $outArr1);
        exec('git commit '.$option.' '.
            //'--allow-empty-message '.
            (config::$authorName?'--author="'.config::$authorName.'" ':'').
            '"'.implode('" "', $_POST['file']).'" '.
            '-m "'.addslashes($_POST['message']).
            '" 2>&1', $outArr);
        $_SESSION['alert'] = $outArr;
    }

    public static function push()
    {
        chdir($_SESSION['workDir']);
        exec("git push ".$_SESSION['remote']." ".$_SESSION['branch']." 2>&1", $outArr);
        $_SESSION['alert'] = $outArr;
    }

    public static function pull()
    {
        chdir($_SESSION['workDir']);

        if (is_dir('.git/rebase-apply')) {
            exec("git rebase --continue 2>&1", $outArr);
        } else {
            exec("git pull --rebase ".$_SESSION['remote']." ".$_SESSION['branch']." 2>&1", $outArr);
        }

        $_SESSION['alert'] = $outArr;
    }

    public static function diff()
    {
        chdir($_SESSION['workDir']);
        exec('git diff --word-diff '.self::makeFileString().' 2>&1', $outArr);
        $_SESSION['dialog'] = $outArr;
    }

    public static function diffajax()
    {
        chdir($_SESSION['workDir']);
        exec('git diff --word-diff '.($filestr=self::makeFileString()).' 2>&1', $outArr);
        $content = '';
        if (!empty($outArr)) {
            foreach ($outArr as $dialogEl) {
                $content .= preg_replace(array('/\[-(.*)-\]/U', '/{\+(.*)\+}/U'),
                    array('<span class="deleted">\1</span>', '<span class="added">\1</span>'),
                    $dialogEl).'<br>';
            }
        }

        echo json_encode(array(
            'content' => $content,
            'title'   => $filestr,
        ));
        exit;
    }

    public static function diffnull()
    {
        chdir($_SESSION['workDir']);
        exec('git diff --word-diff /dev/null '.self::makeFileString().' 2>&1', $outArr);
        $_SESSION['dialog'] = $outArr;
    }

    public static function checkout()
    {
        chdir($_SESSION['workDir']);
        exec('git checkout '.self::makeFileString().' 2>&1', $outArr);
        $_SESSION['alert'] = $outArr;
    }

    public static function ours()
    {
        chdir($_SESSION['workDir']);
        exec('git checkout -2 '.self::makeFileString().' 2>&1', $outArr);
        $_SESSION['alert'] = $outArr;
        exec('git add '.self::makeFileString().' 2>&1', $outArr);
        $_SESSION['alert'] = array_merge($_SESSION['alert'], $outArr);
        exec('git commit -i '.
            '--allow-empty-message '.
            (config::$authorName?'--author="'.config::$authorName.'" ':'').
            self::makeFileString().' '.
            '-m "" 2>&1', $outArr);
    }

    public static function theirs()
    {
        chdir($_SESSION['workDir']);
        exec('git checkout -3 '.self::makeFileString().' 2>&1', $outArr);
        $_SESSION['alert'] = $outArr;
        exec('git add '.self::makeFileString().' 2>&1', $outArr);
        $_SESSION['alert'] = array_merge($_SESSION['alert'], $outArr);
        exec('git commit -i '.
            '--allow-empty-message '.
            (config::$authorName?'--author="'.config::$authorName.'" ':'').
            self::makeFileString().' '.
            '-m "" 2>&1', $outArr);
    }

    public static function delete()
    {
        chdir($_SESSION['workDir']);

        if (empty($_POST['file']) && !empty($_GET['file']) &&
            strpos(realpath($_GET['file']), $_SESSION['workDir']) !== false ) {
            unlink($_GET['file']);
        }
    }

    public static function resolve()
    {
        chdir($_SESSION['workDir']);
        exec('git add '.self::makeFileString().' 2>&1', $outArr);
        $_SESSION['alert'] = $outArr;
    }

}
