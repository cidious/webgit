(function ($) {
    $(function() {
        if ($('#topalert-text').html() != '') {
            $('#topalert').show();
        }
        if ($('div.modal div.modal-body').html() != '') {
            $('div.modal').modal('show');
        }
        $('.checkFileAll').change(function() {
            if ($(this).is(':checked')) {
                $('.checkFile').attr({checked: 'checked'});
            } else {
                $('.checkFile').removeAttr('checked');
            }
        });
        $('.select-project').change(function() {
            window.location = 'processState.php?action=project&name='+$(this).val();
        });
        $('a.delete').click(function() {
            if (confirm('Are you sure to delete this file?')) {
                return true;
            } else {
                return false;
            }
        });
        $('#inputMessage').change(function() {
            if ('' == $(this).val()) {
                $('#buttonCommit').attr({disabled:'disabled'});
                $('#buttonCheckout').attr({disabled:'disabled'});
            } else {
                $('#buttonCommit').removeAttr('disabled');
                $('#buttonCheckout').removeAttr('disabled');
            }
        });
        $('#inputMessage').change();
        $('button.diff').click(function() {
            $.getJSON($(this).data('href'), {}, function(data) {
                $('div.modal').modal('show');
                $('div.modal .modal-header h3').html(data.title);
                $('div.modal .modal-body').html(data.content);
            });




        });
    });
})(jQuery);