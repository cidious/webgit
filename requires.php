<?php
/**
 * requires.php
 * Date: 1/11/13
 * Time: 10:00 PM
 */

error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();

if (!is_readable('config.php')) {
    die('Please create config.php (cp config.php.sample config.php)');
}

require_once 'config.php';
require_once 'classes/libgit.php';
